import node_list as List
# Function to count the frequency of each character in the text
def count_frequencies(text):
    frequencies = {}
    for char in text: 
        #check if char exist in frequencies
        if char in frequencies:
            frequencies[char] += 1
        else:
            frequencies[char] = 1
    return frequencies

# Function to create a list of nodes sorted by their frequency
def create_sorted_node_list(frequencies):   
    my_list = List.LinkedList()
    for symbol, frequency in frequencies.items():
        node = {'symbol': symbol, 'frequency': frequency}  
        my_list.add(node); 
    
    my_list.print();
    return 

# Function to build the Huffman tree
def build_huffman_tree(node_list):
# Insert code here
    return node_list[0]


# Function to generate Huffman codes for each symbol
def generate_huffman_codes(node, code, codes): 
     # Insert code herezzz
     return


# Function to encode the text using Huffman codes
def encode_text(text, codes):
# Insert code here
    return 

# Function to save the encoded text and Huffman tree to files
def compress_and_save(input_file_path, output_file_path, frequencies_file_path): 
    return


# لا تغيير بعد هذا السطر
input_file_path = 'input.txt'
output_file_path = 'compressed_text.txt'
frequencies_file_path = 'frequencies.txt'
compress_and_save(input_file_path, output_file_path, frequencies_file_path)
  

 #tests
str = input("Enter Text");    
print(count_frequencies(str));
create_sorted_node_list(frequencies= count_frequencies(str));