class Node: 
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList: 
    #first = Node ;
    def __init__(self):
        self.first = None

    def add(self, data):
         #Add data to Node 
        new_node = Node(data) 
        #first Node
        if self.first is None:
            self.first = new_node
        else:
            current = self.first
            while current.next:
                current = current.next
            current.next = new_node
    def sortedList (): 
        return
    def print(self):
        current = self.first
        while current:
            print(current.data, end=" ")
            current = current.next
        print()